package com.example.listbuku.databasebuku

import android.content.Context
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class dtbbuku(
    @PrimaryKey(autoGenerate = true)
    val id: Int, val judul: String, val penulis: String
)
